{
    up:[
        {
            create_table:{
                name:"tblParameter",
                columnPrefix:"fldParameter_",
                columns:[
                    {name:"Code",  type:"int",    primaryKey:true},
                    {name:"Value", type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblParameter"
    }
}