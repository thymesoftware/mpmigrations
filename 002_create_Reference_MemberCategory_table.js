{
    up:[
        {
            create_table:{
                name:"tblReference_MemberCategory",
                columnPrefix:"fldMemberCategory_",
                columns:[
                    {name:"ID",         type:"int",     primaryKey:true},
                    {name:"Name",       type:"string"},
                    {name:"IsEnabled",  type:"boolean"},
                    {name:"ClassCode",  type:"int"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblReference_MemberCategory"
    }
}