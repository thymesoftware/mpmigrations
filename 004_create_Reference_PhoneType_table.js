{
    up:[
        {
            create_table:{
                name:"tblReference_PhoneType",
                columnPrefix:"fldPhoneType_",
                columns:[
                    {name:"ID",         type:"int",     primaryKey:true},
                    {name:"Name",       type:"string"},
                    {name:"IsEnabled",  type:"boolean"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblReference_PhoneType"
    }
}