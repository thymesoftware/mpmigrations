{
    up:[
        {
            create_table:{
                name:"tblReference_SubAccount",
                columnPrefix:"fldSubAccount_",
                columns:[
                    {name:"ID",             type:"int",     primaryKey:true},
                    {name:"Name",           type:"string"},
                    {name:"IsEnabled",      type:"boolean"},
                    {name:"ModeCode",       type:"int"},
                    {name:"Sign",           type:"smallint"},
                    {name:"AvailableFrom",  type:"string"},
                    {name:"ExpiresAfter",   type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblReference_SubAccount"
    }
}