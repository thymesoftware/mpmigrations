{
    up:[
        {
            create_table:{
                name:"tblMember",
                columnPrefix:"fldMember_",
                columns:[
                    {name:"ID",              type:"int",     primaryKey:true},
                    {name:"Number",          type:"string"},
                    {name:"IsEnabled",       type:"boolean"},
                    {name:"RoleCode",        type:"int"},
                    {name:"Password",        type:"string"},
                    {name:"Name_Surname",    type:"string"},
                    {name:"Name_Given",      type:"string"},
                    {name:"Name_Preferred",  type:"string"},
                    {name:"Name_Title",      type:"string"},
                    {name:"Name_Honour",     type:"string"},
                    {name:"Name_Display",    type:"string"},
                    {name:"Name_Filing",     type:"string"},
                    {name:"GenderCode",      type:"int"},
                    {name:"DateOfBirth",     type:"date"},
                    {name:"Email",           type:"string"},
                    {name:"Email_Silent",    type:"string"},
                    {name:"GolfLinkNumber",  type:"string"},
                    {name:"CategoryID",      type:"int"},
                    {name:"IsFinancial",     type:"boolean"}, 
                    {name:"IsPlayingMember", type:"boolean"}, 
                    {name:"OwingBalance_Total",        type:"currency"},
                    {name:"OwingBalance_AgedColumn_0", type:"currency"},
                    {name:"OwingBalance_AgedColumn_1", type:"currency"},
                    {name:"OwingBalance_AgedColumn_2", type:"currency"},
                    {name:"OwingBalance_AgedColumn_3", type:"currency"},
                    {name:"OwingBalance_AgedColumn_4", type:"currency"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember"
    }
}