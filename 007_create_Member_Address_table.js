{
    up:[
        {
            create_table:{
                name:"tblMember_Address",
                columnPrefix:"fldMemberAddress_",
                columns:[
                    {name:"MemberID",       type:"int",     primaryKey:true},
                    {name:"AddressTypeID",  type:"int",     primaryKey:true},
                    {name:"Line_1",         type:"string"},
                    {name:"Line_2",         type:"string"},
                    {name:"Line_3",         type:"string"},
                    {name:"Line_4",         type:"string"},
                    {name:"Suburb",         type:"string"},
                    {name:"State",          type:"string"},
                    {name:"PostCode",       type:"string"},
                    {name:"Country",        type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember_Address"
    }
}