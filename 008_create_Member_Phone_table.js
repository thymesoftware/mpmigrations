{
    up:[
        {
            create_table:{
                name:"tblMember_Phone",
                columnPrefix:"fldMemberPhone_",
                columns:[
                    {name:"MemberID",       type:"int",     primaryKey:true},
                    {name:"PhoneTypeID",    type:"int",     primaryKey:true},
                    {name:"Number",         type:"string"},
                    {name:"Number_Silent",  type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember_Phone"
    }
}