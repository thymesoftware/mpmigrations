{
    up:[
        {
            create_table:{
                name:"tblMember_Balance",
                columnPrefix:"fldMemberBalance_",
                columns:[
                    {name:"MemberID",        type:"int",     primaryKey:true},
                    {name:"SubAccountID",    type:"int",     primaryKey:true},
                    {name:"CurrentBalance",  type:"currency"},
                    {name:"ValidOn",         type:"datetime"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember_Balance"
    }
}