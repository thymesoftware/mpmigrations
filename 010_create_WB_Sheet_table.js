{
    up:[
        {
            create_table:{
                name:"tblWB_Sheet",
                columnPrefix:"fldWB_Sheet_",
                columns:[
                    {name:"ID",               type:"guid",    primaryKey:true},
                    {name:"Date",             type:"date"},
                    {name:"Title",            type:"string"},
                    {name:"Description",      type:"string"},
                    {name:"Period",           type:"int"},
                    {name:"IsTemplate",       type:"boolean"},
                    {name:"IsHidden",         type:"boolean"},
                    {name:"IsLocked",         type:"boolean"},
                    {name:"Open_DateOffset",  type:"int"},
                    {name:"Open_Time",        type:"time"},
                    {name:"Close_DateOffset", type:"int"},
                    {name:"Close_Time",       type:"time"},
                
                    {name:"Limit_MaxBookingsPerMember", type:"int"},
                    {name:"Limit_MaxGroupsPerMember",   type:"int"},

                    {name:"RestrictionSetID",           type:"int"}
                ],
            
                indexedColumns:["Date", "IsTemplate"]
            }
        }
    ],
    
    down:{
        drop_table:"tblWB_Sheet"
    }
}