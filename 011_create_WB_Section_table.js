{
    up:[
        {
            create_table:{
                name:"tblWB_Section",
                columnPrefix:"fldWB_Section_",
                columns:[
                    {name:"ID",               type:"guid",    primaryKey:true},
                    {name:"SheetID",          type:"guid"},
                    {name:"Sort",             type:"int"},
                    {name:"Type",             type:"int"},
                    {name:"GroupSize",        type:"int"},
                    {name:"Title",            type:"string"},
                    {name:"Description",      type:"string"},
                    {name:"Deleted",          type:"bool"},
                    {name:"RestrictionSetID", type:"int"}
                ],
            
                indexedColumns:["SheetID"]
            }        
        }
    ],
    
    down:{
        drop_table:"tblWB_Section"
    }
}