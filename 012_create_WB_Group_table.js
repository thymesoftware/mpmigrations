{
    up:[
        {
            create_table:{
                name:"tblWB_Group",
                columnPrefix:"fldWB_Group_",
                columns:[
                    {name:"ID",               type:"guid",    primaryKey:true},
                    {name:"SheetID",          type:"guid"},
                    {name:"SectionID",        type:"guid"},
                    {name:"Sort",             type:"int"},
                    {name:"TeeOffTime",       type:"time"},
                    {name:"TeeOffHole",       type:"int"},
                    {name:"Title",            type:"string"},
                    {name:"Deleted",          type:"bool"},
                    {name:"RestrictionSetID", type:"int"}
                ],
            
                indexedColumns:["SheetID", "SectionID"]
            }
        }
    ],
    
    down:{
        drop_table:"tblWB_Group"
    }
}