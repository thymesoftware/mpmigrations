{
    up:[
        {
            create_table:{
                name:"tblWB_Booking",
                columnPrefix:"fldWB_Booking_",
                columns:[
                    {name:"ID",              type:"guid",      primaryKey:true},
                    {name:"SheetID",         type:"guid"},
                    {name:"GroupID",         type:"guid"},

                    {name:"Sequence",        type:"auto",      nullable:false},
                    {name:"Deleted",         type:"boolean"},

                    {name:"PositionInGroup", type:"int"},
                    {name:"Type",            type:"int"},
                    {name:"CreatedBy",       type:"int"},
                    {name:"CreatedOn",       type:"datetime"},
                    {name:"MemberID",        type:"int"},
                    
                    {name:"Guest_Name",            type:"string"},             
                    {name:"Guest_Gender",          type:"int"},
                    {name:"Guest_GLNumber",        type:"string"},
                    {name:"Guest_PlayingHandicap", type:"int"},
                    {name:"Guest_ClubName",        type:"string"},

                    {name:"Reservation_Name",      type:"string"}
                ],
            
                indexedColumns:["SheetID", "GroupID", "CreatedBy", "MemberID"]
            }
        }
    ],
    
    down:{
        drop_table:"tblWB_Booking"
    }
}