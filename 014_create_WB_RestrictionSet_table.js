{
    up:[
        {
            create_table:{
                name:"tblWB_RestrictionSet",
                columnPrefix:"fldWB_RestrictionSet_",
                columns:[
                    {name:"ID",                type:"int",    primaryKey:true},                
                    {name:"Gender",            type:"int"},
                    {name:"FinancialMembers",  type:"int"},
                    {name:"PlayingMembers",    type:"int"},
                    {name:"OtherMembers",      type:"int"},
                    {name:"Guests",            type:"int"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblWB_RestrictionSet"
    }
}