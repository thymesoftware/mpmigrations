{
    up:[
        {
            create_table:{
                name:"tblMember_Statement",
                columnPrefix:"fldMemberStatement_",
                columns:[
                    {name:"MemberID",   type:"int",      primaryKey:true},
                    {name:"DocumentIndex", type:"int",   primaryKey:true},
                    {name:"StatementDate", type:"date"},
                    {name:"FromDate", type:"date"},
                    {name:"ToDate", type:"date"},
                    {name:"IsRenewal", type:"bool"},
                    {name:"OwingBalance", type:"decimal"},
                    {name:"FilePath",   type:"string"},
                    {name:"FileName",   type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember_Statement"
    }
}
