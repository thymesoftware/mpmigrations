{
    up:[
        {
            create_table:{
                name:"tblMember_Partner",
                columnPrefix:"fldMemberPartner_",
                columns:[
                    {name:"MemberID",                    type:"int",     primaryKey:true},
                    {name:"Sort",                        type:"int",     primaryKey:true},
                    {name:"Partner_MemberID",            type:"int"},
                    {name:"Partner_GuestGolfLinkNumber", type:"string"},
                    {name:"Partner_GuestName",           type:"string"},
                    {name:"Partner_GuestGenderCode",     type:"int"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblMember_Partner"
    }
}
