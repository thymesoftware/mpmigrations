{
    up:[
        {
            create_table:{
                name:"tblJournal",
                columnPrefix:"fldJournal_",
                columns:[
                    {name:"ID",                 type:"auto",    primaryKey:true},
                    {name:"UserID",             type:"int"},
                    {name:"TimeStamp",          type:"datetime"},
                    {name:"OperationCode",      type:"int"},
                    {name:"Reference_MemberID", type:"int"},
                    {name:"Reference_AuxID",    type:"int"},
                    {name:"OldValue",           type:"string"},
                    {name:"NewValue",           type:"string"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblJournal"
    }
}