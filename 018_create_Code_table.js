{
    up:[
        {
            create_table:{
                name:"tblCode",
                columnPrefix:"fldCode_",
                columns:[
                    {name:"ID",      type:"int",    primaryKey:true},
                    {name:"GroupID", type:"int"},
                    {name:"Sort", type:"int"},
                    {name:"Meaning", type:"string"}
                ]
            }
        },

        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning"],
                values:[
                    [1,1,null,"Code groups"],
                    [2,1,null,"Genders"],
                    [3,1,null,"Member roles"],
                    [4,1,null,"Country codes"],
                    [6,1,null,"Member-category classes"],
                    [9,1,null,"Sub-account modes"],
                    [10,1,null,"Journal operations"],
                    [999,1,null,"Parameters"],
                    [1001,2,null,"Male"],
                    [1002,2,null,"Female"],
                    [1051,4,null,"Australia"],
                    [1052,4,null,"New Zealand"],
                    [1053,4,null,"Papua New Guinea"],
                    [1100,3,null,"Member"],
                    [1119,3,null,"Administrator"],
                    [1150,9,null,"Reserved account"],
                    [1151,9,null,"Membership account"],
                    [1152,9,null,"PoS Charge account"],
                    [1153,9,null,"Deposit account"],
                    [1154,9,null,"PoS Levy account"],
                    [1155,9,null,"PoS Prize account"],
                    [1156,9,null,"Loyalty account"],
                    [1200,6,null,"Club member"],
                    [1201,6,null,"Visitor"],
                    [1219,6,null,"Non-member"],
                    [2001,10,null,"Member changed surname"],
                    [2002,10,null,"Member changed given names"],
                    [2003,10,null,"Member changed preferred name"],
                    [2004,10,null,"Member changed title"],
                    [2005,10,null,"Member changed honour"],
                    [2006,10,null,"Member changed date of birth"],
                    [2015,10,null,"Member changed phone number"],
                    [2016,10,null,"Member changed phone is silent"],
                    [2021,10,null,"Member changed address line 1"],
                    [2022,10,null,"Member changed address line 2"],
                    [2023,10,null,"Member changed address line 3"],
                    [2024,10,null,"Member changed address line 4"],
                    [2025,10,null,"Member changed address suburb"],
                    [2026,10,null,"Member changed address state"],
                    [2027,10,null,"Member changed address post code"],
                    [2028,10,null,"Member changed address country"],
                    [2080,10,null,"Member acknowledged notification"],
                    [2091,10,null,"Member logged in"],
                    [2092,10,null,"Member logged out"],
                    [2093,10,null,"Member changed password"],
                    [2094,10,null,"Member login failed"],
                    [2095,10,null,"Attempted login by a disabled member"],
                    [2096,10,null,"First login by a member"],
                    [9001,999,null,"Club ID"],
                    [9002,999,null,"Club name"],
                    [9003,999,null,"Club country code"],
                    [9004,999,null,"Club home state"],
                    [9005,999,null,"Is a golf club"],
                    [9021,999,null,"What the member \"code\" is called at this club"],
                    [9022,999,null,"Enable silent member option"],
                    [9030,999,null,"Aged owing balance column headings 0"],
                    [9031,999,null,"Aged owing balance column headings 1"],
                    [9032,999,null,"Aged owing balance column headings 2"],
                    [9033,999,null,"Aged owing balance column headings 3"],
                    [9034,999,null,"Aged owing balance column headings 4"],
                    [9039,999,null,"Number of aged owing balance columns to show"],
                    
                    [101,1,null,"Booking sheet periods"],
                    [102,1,null,"Booking sheet gender restrictions"],
                    [103,1,null,"Booking sheet financial member restrictions"],
                    [104,1,null,"Booking sheet other member restrictions"],
                    [105,1,null,"Booking sheet guest restrictions"],
                    [5000,101,1,"All day"],
                    [5001,101,2,"Morning"],
                    [5002,101,3,"Afternoon"],
                    [5003,101,4,"Twilight"],
                    [5100,102,null,"No gender restriction"],
                    [5101,102,null,"Allow male players only"],
                    [5102,102,null,"Allow female players only"],
                    [5120,103,null,"No financial status restriction"],
                    [5121,103,null,"Allow financial members only"],
                    [5140,104,null,"Do not allow booking of other members"],
                    [5159,104,null,"Allow booking of other members"],
                    [5160,105,null,"Do not allow booking of guests"],
                    [5179,105,null,"Allow booking of guests"],
                    
                    [106,1,null,"Booking sheet section types"],
                    [5180,106,null,"Golf Normal tee-off"],
                    [5181,106,null,"Golf Shotgun tee-off"],

                    [150,1,null,"Booking types"],
                    [5200,150,null,"Member booking"],
                    [5201,150,null,"Guest booking"],
                    [5210,150,null,"Reserved"],

                    [9901,999,null,"Database Read-Only mode"],
                    
                    [107,1,null,"Booking sheet playing member restrictions"],
                    [5130,107,null,"No playing member restriction"],
                    [5131,107,null,"Allow playing members only"]
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblCode"
    }
}