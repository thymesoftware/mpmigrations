{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning"],
                values:[
                    [9010,999,null,"SMTP Server"],
                    [9011,999,null,"Booking Email From Address"],
                    [9012,999,null,"Booking Email From Name"],

                    [9902,999,null,"Demo/Test mode"],
                    [9903,999,null,"Demo/Test Destination Email Address Override"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9010,
                  9011,
                  9012,
                  9902,
                  9903
                ]
            }
        }
    ]
}