{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning"],
                values:[
                    [5,1,null,"Default password modes"],
                    [1010,5,null,"DOB: ddmm"],
                    [1011,5,null,"DOB: ddmmyy"],
                    [1012,5,null,"DOB: ddmmyyyy"],
                    [9006,999,null,"Default Password Mode"],
                    [9904,999,null,"Use Member Number As Authentication Base"]                    
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                5,
                1010,
                1011,
                1012,
                9006,
                9904
              ]
            }
        }
    ]
}