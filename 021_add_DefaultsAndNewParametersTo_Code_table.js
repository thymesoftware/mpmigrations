{
    up:[
        {
            add_column:{
              table:"tblCode",
              columns:[
                {name:"fldCode_DefaultValue", type:"string"}
              ]
            }
        },

        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue"],
                values:[
                    [9905,999,null,"MemberDirectoryEmailMode","0"],
                    [9906,999,null,"MemberDirectoryPhoneMode","0"],                   
                    [9910,999,null,"ShowUserPanel","1"],                   
                    [9911,999,null,"MemberAccountEnabled","1"],                   
                    [9912,999,null,"MemberDirectoryEnabled","1"],                   
                    [9913,999,null,"BookingsEnabled","1"],                   
                    [9914,999,null,"ResultsEnabled","1"]                   
                ]
            }
        },

        {
            execute:"UPDATE tblCode SET fldCode_DefaultValue = 'F' WHERE fldCode_ID = 9901;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_DefaultValue = 'F' WHERE fldCode_ID = 9902;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_DefaultValue = 'info@thymesoft.com.au' WHERE fldCode_ID = 9903;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_DefaultValue = 'F' WHERE fldCode_ID = 9904;"
        }



    ],
    
    down:[
        {
            remove_column:{
                table:"tblCode",
                columnName:"fldCode_DefaultValue"
            }  
        },
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                9905,
                9906,
                9910,
                9911,
                9912,
                9913,
                9914
              ]
            }
        }
    ]
}