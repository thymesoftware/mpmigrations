{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning"],
                values:[
                    [1013,5,null,"MM Provided"],
                    [9000,999,null,"Club code"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                1013,
                9000
              ]
            }
        }
    ]
}