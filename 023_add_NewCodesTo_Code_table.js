{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue"],
                values:[
                    [1116,3,null,"Proshop Staff",null],
                    [1117,3,null,"Volunteer",null],
                    [1118,3,null,"Club Staff",null],
                    [1014,5,null,"Surname",null],
                    [9915,999,null,"DisableMemberPasswordChange","0"],
                    [9040,999,null,"GuestBookingsRequireGLNum","0"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                1116,
                1117,
                1118,
                1014,
                9915,
                9040
              ]
            }
        }
    ]
}