{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue"],
                values:[
                    [9909,999,null,"Site Mode","0"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                9909
              ]
            }
        }
    ]
}