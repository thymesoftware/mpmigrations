{
    up:[
        {
            add_column:{
              table:"tblCode",
              columns:[
                {name:"fldCode_MMSync", type:"boolean"}
              ]
            }
        },

        {
            add_column:{
              table:"tblCode",
              columns:[
                {name:"fldCode_Options", type:"string"}
              ]
            }
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9000;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1, fldCode_Options = '1' WHERE fldCode_ID = 9001;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9002;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9003;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9004;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1, fldCode_Options = '2' WHERE fldCode_ID = 9005;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '3|MemberPortal.Core.Code_DefaultPasswordMode' WHERE fldCode_ID = 9006;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9021;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9022;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9030;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9031;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9032;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9033;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9034;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1, fldCode_Options = '1' WHERE fldCode_ID = 9039;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '2' WHERE fldCode_ID = 9040;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '2' WHERE fldCode_ID = 9901;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1, fldCode_Options = '2' WHERE fldCode_ID = 9902;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9903;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '3|MemberPortal.Core.Code_MmMemberDirectoryEmailMode' WHERE fldCode_ID = 9905;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '3|MemberPortal.Core.Code_MmMemberDirectoryPhoneMode' WHERE fldCode_ID = 9906;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '3|MemberPortal.Core.Code_MmSiteMode' WHERE fldCode_ID = 9909;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '3|MemberPortal.Core.Code_MmShowUserPanel' WHERE fldCode_ID = 9910;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Options = '2' WHERE fldCode_ID = 9915;"
        }
    ],
    
    down:[
      {
        remove_column:{
          table:"tblCode",
          columnName:"fldCode_MMSync"
        }  
      },
      {
        remove_column:{
          table:"tblCode",
          columnName:"fldCode_Options"
        }  
      }        
    ]
}