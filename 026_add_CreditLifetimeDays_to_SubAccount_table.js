{
    up:[
        {
            add_column:{
              table:"tblReference_SubAccount",
              columns:[
                {name:"fldSubAccount_CreditLifetimeDays", type:"int"}
              ]
            }
        }
    ],
    
    down:[
      {
        remove_column:{
          table:"tblReference_SubAccount",
          columnName:"fldSubAccount_CreditLifetimeDays"
        }  
      }
    ]
}