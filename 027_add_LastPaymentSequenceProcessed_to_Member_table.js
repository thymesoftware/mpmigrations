{
    up:[
        {
            add_column:{
              table:"tblMember",
              columns:[
                {name:"fldMember_OwingBalance_ValidOn", type:"datetime"},
                {name:"fldMember_LastPaymentSequenceProcessed", type:"int"}
              ]
            }
        }
    ],
    
    down:[
      {
        remove_column:{
          table:"tblMember",
          columnName:"fldMember_OwingBalance_ValidOn"
        }
      },
      {
        remove_column:{
          table:"tblMember",
          columnName:"fldMember_LastPaymentSequenceProcessed"
        }  
      }
    ]
}