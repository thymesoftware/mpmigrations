{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue","MMSync", "Options"],
                values:[
                    [9041,999,null,"Member balance display: Merge PosCharge with Membership","F","1","2"],
                    [9042,999,null,"Member balance display: Excluded Sub-accounts","","0",""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                9041,
                9042
              ]
            }
        }
    ]
}