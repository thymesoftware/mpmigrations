{
    up:[
        {
            create_table:{
                name:"tblWebPayment",
                columnPrefix:"fldWebPayment_",
                columns:[
                    {name:"ID",              type:"guid",      primaryKey:true},
                    {name:"Sequence",        type:"auto",      nullable:false},
                    {name:"PayingMemberID",  type:"int"},
                    {name:"Date",            type:"date"},
                    {name:"CreatedOn",       type:"datetime"},
                    {name:"ReceiptNumber",   type:"string"},
                    {name:"TotalAmount",     type:"currency"},
                    {name:"SurchargeAmount", type:"currency"},
                    {name:"Note",            type:"string"},
                    {name:"GatewayID",       type:"int"},
                    {name:"Details",         type:"memo"}
                ],
                        
                indexedColumns:["Sequence"]
            }
        },
        {
            create_table:{
                name:"tblWebPaymentAllocation",
                columnPrefix:"fldWebPaymentAllocation_",
                columns:[
                    {name:"ID",           type:"guid",     primaryKey:true},
                    {name:"PaymentID",    type:"guid"},
                    {name:"LineNumber",   type:"int"},
                    {name:"Type",         type:"int"},
                    {name:"Amount",       type:"currency"},
                    {name:"PaidMemberID", type:"int"},
                    {name:"SubAccount",   type:"int"},
                    {name:"Details",      type:"memo"}
                ],
                        
                indexedColumns:["PaymentID"]
            }
        },
        {
            create_table:{
                name:"tblWebPayment_Pending",
                columnPrefix:"fldWebPayment_Pending_",
                columns:[
                    {name:"ID",              type:"guid",      primaryKey:true},
                    {name:"Status",          type:"int"},
                    {name:"PayingMemberID",  type:"int"},
                    {name:"Date",            type:"date"},
                    {name:"CreatedOn",       type:"datetime"},
                    {name:"ReceiptNumber",   type:"string"},
                    {name:"TotalAmount",     type:"currency"},
                    {name:"SurchargeAmount", type:"currency"},
                    {name:"Note",            type:"string"},
                    {name:"GatewayID",       type:"int"},
                    {name:"Details",         type:"memo"}
                ],
                        
                indexedColumns:["Status"]
            }
        },
        {
            create_table:{
                name:"tblWebPayment_PendingAllocation",
                columnPrefix:"fldWebPayment_PendingAllocation_",
                columns:[
                    {name:"ID",           type:"guid",     primaryKey:true},
                    {name:"PaymentID",    type:"guid"},
                    {name:"LineNumber",   type:"int"},
                    {name:"Type",         type:"int"},
                    {name:"Amount",       type:"currency"},
                    {name:"PaidMemberID", type:"int"},
                    {name:"SubAccount",   type:"int"},
                    {name:"Details",      type:"memo"}
                ],
                        
                indexedColumns:["PaymentID"]
            }
        }
    ],
    
    down:[
        {
            drop_table:"tblWebPayment"
        },
        {
            drop_table:"tblWebPaymentAllocation"
        },
        {
            drop_table:"tblWebPayment_Pending"
        },
        {
            drop_table:"tblWebPayment_PendingAllocation"
        }
    ]
}
