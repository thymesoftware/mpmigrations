{
	up: [
    {
		  add_column: {
			  table: "tblMember_Balance",
			  columns: [
          {
				    name: "fldMemberBalance_ToEndOfLastMonth",
				    type: "currency"
			    },
			    {
				    name: "fldMemberBalance_ThisMonthToDate",
				    type: "currency"
			    },
			    {
				    name: "fldMemberBalance_NearFuture",
				    type: "currency"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblMember_Balance",
			  columnName: "fldMemberBalance_ToEndOfLastMonth"
		  }
	  },
	  {
		  remove_column: {
			  table: "tblMember_Balance",
			  columnName: "fldMemberBalance_ThisMonthToDate"
		  }
	  },
	  {
		  remove_column: {
			  table: "tblMember_Balance",
			  columnName: "fldMemberBalance_NearFuture"
		  }
	  }
  ]
}
