{
	up: [
    {
		  add_column: {
			  table: "tblMember_Statement",
			  columns: [
          {
				    name: "fldMemberStatement_ClosingBalancesBySubAccount",
				    type: "string"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblMember_Statement",
			  columnName: "fldMemberStatement_ClosingBalancesBySubAccount"
		  }
	  }
  ]
}
