﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9100,999,null,"PayPal Sandbox Mode", "T", 0, ""],
                    [9101,999,null,"PayPal API Username", "", 0, ""],
                    [9102,999,null,"PayPal API Password", "", 0, ""],
                    [9103,999,null,"PayPal API Signature", "", 0, ""],
                    [9104,999,null,"PayPal Third-Party Email - Only set if using Thyme API credentials", "", 0, ""],
                    [9105,999,null,"PayPal Header Image URL - HTTPS ONLY", "", 0, ""],
                    [9106,999,null,"PayPal Logo Image URL - HTTPS ONLY - Overrides the Header Image URL", "", 0, ""],
                    [9043,999,null,"Number of Statements to Keep", "", 1, "1"],
                    [9044,999,null,"Account Balances Enabled", "", 1, "2"],
                    [9045,999,null,"Payments Enabled", "", 1, "2"],
                    [9046,999,null,"Default Payment Amounts", "0", 0, "3|MemberPortal.Core.Code_DefaultPaymentAmounts"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9100,
                  9101,
                  9102,
                  9103,
                  9104,
                  9105,
                  9106,
                  9043,
                  9044,
                  9045,
                  9046
                ]
            }
        }
    ]
}