{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning"],
                values:[
                    [108,1,null,"Booking sheet member days-can-play restrictions"],
                    [5190,108,null,"Member days-can-play restrictions enforced"],
                    [5191,108,null,"Member days-can-play restrictions NOT enforced"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
              table_name:"tblCode",
              compare_column:"fldCode_ID",
              values:[
                108,
                5190,
                5191
              ]
            }
        }
    ]
}