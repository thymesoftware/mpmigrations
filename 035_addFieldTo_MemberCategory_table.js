{
	up: [
    {
		  add_column: {
			  table: "tblReference_MemberCategory",
			  columns: [
          {
				    name: "fldMemberCategory_GM_RuleDetails",
				    type: "memo"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblReference_MemberCategory",
			  columnName: "fldMemberCategory_GM_RuleDetails"
		  }
	  }
  ]
}
