{
    up:[
        {
            create_table:{
                name:"tblReference_MemberGroup",
                columnPrefix:"fldMemberGroup_",
                columns:[
                    {name:"ID",         type:"int",     primaryKey:true},
                    {name:"Name",       type:"string"},
                    {name:"IsDeleted",  type:"boolean"},
                    {name:"IsHeading",  type:"boolean"},
                    {name:"HeadingID",  type:"int"},
                    {name:"SortOrder",  type:"int"}
                ]
            }
        }
    ],
    
    down:{
        drop_table:"tblReference_MemberGroup"
    }
}