{
	up: [
    {
		  add_column: {
			  table: "tblMember",
			  columns: [
          {
				    name: "fldMember_GroupIdList",
				    type: "memo"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_GroupIdList"
		  }
	  }
  ]
}
