﻿{
    up:[
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EdmMetadata') DROP TABLE dbo.EdmMetadata"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail_Body') DROP TABLE tblEmail_Body"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail_Recipient') DROP TABLE tblEmail_Recipient"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail') DROP TABLE dbo.tblEmail"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail_Batch') DROP TABLE dbo.tblEmail_Batch"
        },
        {
          execute: "CREATE TABLE [dbo].[EdmMetadata]([Id] [int] IDENTITY(1,1) NOT NULL, [ModelHash] [nvarchar](max) NULL, PRIMARY KEY CLUSTERED ([Id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]"
        },
        {
          execute: "CREATE TABLE [dbo].[tblEmail_Batch]([EmailBatchId] [int] IDENTITY(1,1) NOT NULL, [completed] [bit] NOT NULL, [totalEmails] [int] NOT NULL, [batchCreation] [datetime] NULL, [batchCompletion] [datetime] NULL, [groupsSentTo] [nvarchar](max) NULL, PRIMARY KEY CLUSTERED([EmailBatchId] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]"
        },
        {
          execute: "CREATE TABLE [dbo].[tblEmail]([EmailId] [int] IDENTITY(1,1) NOT NULL, [memberID] [int] NOT NULL,[newsletterId] [int] NOT NULL,[isMember] [bit] NOT NULL,[emailAddress] [nvarchar](max) NULL,[fullName] [nvarchar](max) NULL, [htmlBody] [nvarchar](max) NULL, [textBody] [nvarchar](max) NULL, [subject] [nvarchar](max) NULL, [attachmentPath] [nvarchar](max) NULL, [sent] [bit] NOT NULL, [sendFail] [bit] NOT NULL, [numTries] [int] NOT NULL, [messageType] [nvarchar](max) NULL, [deliveryStatusMessage] [nvarchar](max) NULL, [highPriority] [bit] NOT NULL, [inBatch] [bit] NOT NULL, [dateSent] [datetime] NULL, [EmailBatch_EmailBatchId] [int] NULL, PRIMARY KEY CLUSTERED ([EmailId] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]"
        },
        {
          execute: "ALTER TABLE [dbo].[tblEmail]  WITH CHECK ADD  CONSTRAINT [EmailBatch_theEmails] FOREIGN KEY([EmailBatch_EmailBatchId]) REFERENCES [dbo].[tblEmail_Batch] ([EmailBatchId])"
        },
        {
          execute: "ALTER TABLE [dbo].[tblEmail] CHECK CONSTRAINT [EmailBatch_theEmails]"
        }
    ],
    down:[
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'EdmMetadata') DROP TABLE dbo.EdmMetadata"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail') DROP TABLE dbo.tblEmail"
        },
        {
          execute:"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tblEmail_Batch') DROP TABLE dbo.tblEmail_Batch"
        }
    ]
}