{
	up: [
    {
		  add_column: {
			  table: "tblMember",
			  columns: [
          {
				    name: "fldMember_ReceivesCorrespondence",
				    type: "boolean"
			    },
          {
				    name: "fldMember_EmailCorrespondence",
				    type: "boolean"
			    },
          {
				    name: "fldMember_EmailFinancial",
				    type: "boolean"
			    },
          {
				    name: "fldMember_Status",
				    type: "int"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_ReceivesCorrespondence"
		  }
    },
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_EmailCorrespondence"
		  }
    },
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_EmailFinancial"
		  }
    },
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_Status"
		  }
	  }
  ]
}
