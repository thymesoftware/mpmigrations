{
	up: [
    {
		  add_column: {
			  table: "tblMember",
			  columns: [
          {
				    name: "fldMember_HideInDirectory",
				    type: "boolean"
			    }
        ]
		  }
	  },
    {
		  add_column: {
			  table: "tblReference_MemberCategory",
			  columns: [
          {
				    name: "fldMemberCategory_ShowInDirectory",
				    type: "boolean"
			    },
          {
				    name: "fldMemberCategory_AllowLogin",
				    type: "boolean"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblMember",
			  columnName: "fldMember_HideInDirectory"
		  }
    },
    {
		  remove_column: {
			  table: "tblReference_MemberCategory",
			  columnName: "fldMemberCategory_ShowInDirectory"
		  }
    },
    {
		  remove_column: {
			  table: "tblReference_MemberCategory",
			  columnName: "fldMemberCategory_AllowLogin"
		  }
    }
  ]
}
