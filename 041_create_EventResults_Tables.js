{
    up:[
        {
            create_table:{
                name:"tblEventResults",
                columnPrefix:"fldEventResults_",
                columns:[
                    {name:"ContextCode",    type:"int",     primaryKey:true},
                    {name:"ContextID",      type:"int",     primaryKey:true},
                    {name:"DocumentIndex",  type:"int",     primaryKey:true},
                    {name:"Date",           type:"date"},
                    {name:"Name",           type:"string"},
                    {name:"FilePath",       type:"string"},
                    {name:"FileName",       type:"string"},
                    {name:"FileMode",       type:"int"},
                    {name:"BookingSheetID", type:"guid"}
                ],
            
                indexedColumns:["Date"]
            }
        }
    ],
    
    down:{
        drop_table:"tblEventResults"
    }
}