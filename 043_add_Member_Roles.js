﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "Options"],
                values:[
                    [1120,3,null,"Everyone", "", ""],
                    [1121,3,null,"Blogger","",""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  1120,
                  1121
                ]
            }
        }
    ]
}