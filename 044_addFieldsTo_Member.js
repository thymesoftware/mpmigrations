﻿{
    up: [
    {
        add_column: {
            table: "tblMember",
            columns: [
                {
                    name: "fldMember_DateAccepted",
                    type: "date"
                },
                {
                    name: "fldMember_AdditionalYears",
                    type: "int"
                }
            ]
        }
    }
    ],

    down: [
    {
        remove_column: {
            table:"tblMember",
            columnName: "fldMember_DateAccepted"
        },
        remove_column: {
            table:"tblMember",
            columnName: "fldMember_AdditionalYears"
        }
    }
    ]
}