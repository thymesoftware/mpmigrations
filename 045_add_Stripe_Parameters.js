﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9110,999,null,"Stripe Secret Key", "", 0, ""],
                    [9111,999,null,"Stripe Publishable Key", "", 0, ""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9110,
                  9111
                ]
            }
        }
    ]
}