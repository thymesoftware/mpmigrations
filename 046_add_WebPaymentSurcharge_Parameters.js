﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9120,999,null,"Surcharge Spec - PayPal", "", 1, "0"],
                    [9121,999,null,"Surcharge Spec - Stripe Visa/MasterCard", "", 1, "0"],
                    [9122,999,null,"Surcharge Spec - Stripe Other", "", 1, "0"]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9120,
                  9121,
                  9122
                ]
            }
        }
    ]
}