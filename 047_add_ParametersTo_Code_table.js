﻿{
  up:[
      {
        insert_rows:{
          table_name:"tblCode",
          columnPrefix:"fldCode_",
          columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue","MMSync", "Options"],
          values:[
              [9050,999,null,"GolfLink Club Number", "", "1", ""],
              [9051,999,null,"GolfLink Deskey", "", "1", ""]

          ]
        }
      }
  ],
    
  down:[
      {
        delete_rows:{
          table_name:"tblCode",
          compare_column:"fldCode_ID",
          values:[
            9050,
            9051
          ]
        }
      }
  ]
}