﻿{
  up:[
    {
      create_table:{
        name:"tblWBSet_Set",
        columnPrefix:"fldWBSet_Set_",
        columns:[
            {name:"ID",                type:"guid",    primaryKey:true},
            {name:"Name",              type:"string"},
            {name:"StartDate",         type:"date"},
            {name:"ExpiryDate",        type:"date"},
            {name:"DayOfWeek",         type:"int"},
            {name:"DateCreated",       type:"date"},
            {name:"DateLastModified",  type:"date"},
            {name:"GenderRestriction", type:"int"}
        ]
      }
    },
    {
      create_table:{
        name:"tblWBSet_Section",
        columnPrefix:"fldWBSet_Section_",
        columns:[
            {name:"ID",         type:"guid",    primaryKey:true},
            {name:"SetID",      type:"guid"},
            {name:"Sort",       type:"int"},
            {name:"GroupSize",  type:"int"},
            {name:"Title",      type:"string"}
        ],
            
        indexedColumns:["SetID"]
      }        
    },
    {
      create_table:{
        name:"tblWBSet_Group",
        columnPrefix:"fldWBSet_Group_",
        columns:[
            {name:"ID",         type:"guid",    primaryKey:true},
            {name:"SetID",      type:"guid"},
            {name:"SectionID",  type:"guid"},
            {name:"Sort",       type:"int"},
            {name:"Title",      type:"string"}
        ],
            
        indexedColumns:["SetID"]
      }
    },
    {
      create_table:{
        name:"tblWBSet_Entry",
        columnPrefix:"fldWBSet_Entry_",
        columns:[
            {name:"ID",               type:"guid",      primaryKey:true},
            {name:"SetID",            type:"guid"},
            {name:"GroupID",          type:"guid"},
            {name:"SectionID",        type:"guid"},
            {name:"PositionInGroup",  type:"int"},
            {name:"Type",             type:"int"},
            {name:"CreatedBy",        type:"int"},
            {name:"CreatedOn",        type:"datetime"},
            {name:"MemberID",         type:"int"},
                    
            {name:"ReservationName", type:"string"}
        ],
            
        indexedColumns:["SetID"]
      }
    }
  ],
    
  down:[
    {
      drop_table:"tblWBSet_Set"
    },
    {
      drop_table:"tblWBSet_Section"
    },
    {
      drop_table:"tblWBSet_Group"
    },
    {
      drop_table:"tblWBSet_Entry"
    }
  ]
}