﻿{
    up: [
    {
        add_column: {
            table: "tblMember",
            columns: [
                {
                    name: "fldMember_PhotoUpdated",
                    type: "date"
                },
                {
                    name: "fldMember_PhotoExists",
                    type: "bool"
                }
            ]
        }
    }
    ],

    down: [
    {
      remove_column: {
        table:"tblMember",
        columnName: "fldMember_PhotoUpdated"
      }
    },
    {
        remove_column: {
            table:"tblMember",
            columnName: "fldMember_PhotoExists"
        }
    }
    ]
}