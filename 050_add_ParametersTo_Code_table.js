﻿{
  up:[
      {
        insert_rows:{
          table_name:"tblCode",
          columnPrefix:"fldCode_",
          columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue","MMSync", "Options"],
          values:[
              [9052,999,null,"Current Period Start date", "", "1", ""],
              [9053,999,null,"Current Period End date", "", "1", ""]
          ]
        }
      }
  ],
    
  down:[
      {
        delete_rows:{
          table_name:"tblCode",
          compare_column:"fldCode_ID",
          values:[
            9052,
            9053
          ]
        }
      }
  ]
}