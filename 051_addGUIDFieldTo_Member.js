﻿{
    up: [
    {
        add_column: {
            table: "tblMember",
            columns: [
                {
                    name: "fldMember_GUID",
                    type: "guid"
                }
            ]
        }
    }
    ],

    down: [
    {
      remove_column: {
        table:"tblMember",
        columnName: "fldMember_GUID"
      }
    }
    ]
}