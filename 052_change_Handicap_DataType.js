﻿{
    up:[
        {
          execute:"UPDATE tblWB_Booking SET fldWB_Booking_Guest_PlayingHandicap = 99 WHERE fldWB_Booking_Guest_PlayingHandicap > 99;"
        },
        {
          execute:"ALTER TABLE tblWB_Booking ALTER COLUMN fldWB_Booking_Guest_PlayingHandicap DECIMAL(5, 3);"
        }
    ],
    down:[
      {
        execute:"UPDATE tblWB_Booking SET fldWB_Booking_Guest_PlayingHandicap = ROUND(fldWB_Booking_Guest_PlayingHandicap, 0) WHERE fldWB_Booking_Guest_PlayingHandicap != ROUND(fldWB_Booking_Guest_PlayingHandicap, 0);"
      },
      {
        execute:"ALTER TABLE tblWB_Booking ALTER COLUMN fldWB_Booking_Guest_PlayingHandicap INT;"
      }
    ]
}