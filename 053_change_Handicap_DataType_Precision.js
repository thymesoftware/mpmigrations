﻿{
    up:[
        {
          execute:"UPDATE tblWB_Booking SET fldWB_Booking_Guest_PlayingHandicap = 99 WHERE fldWB_Booking_Guest_PlayingHandicap > 99;"
        },
        {
          execute:"ALTER TABLE tblWB_Booking ALTER COLUMN fldWB_Booking_Guest_PlayingHandicap DECIMAL(3, 1);"
        }
    ],
    down:[
      {
        execute:"ALTER TABLE tblWB_Booking ALTER COLUMN fldWB_Booking_Guest_PlayingHandicap DECIMAL(5, 3);"
      }
    ]
}