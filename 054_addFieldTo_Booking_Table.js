﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Booking",
        columns: [
          {
            name: "fldWB_Booking_Guest_EmailAddress",
            type: "string"
          },
          {
            name: "fldWB_Booking_Guest_MobileNumber",
            type: "string"
          },
          {
            name: "fldWB_Booking_Guest_Notes",
            type: "string"
          }
        ]
      }
    }
  ],
  down:[
    {
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_Guest_EmailAddress"
      },
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_Guest_MobileNumber"
      },
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_Guest_Notes"
      }
    }
  ]
}