﻿{
  up: [
    {
      add_column: {
        table: "tblWBSet_Entry",
        columns: [
          {
            name: "fldWBSet_Entry_Guest_Name",
            type: "string"
          },
          {
            name: "fldWBSet_Entry_Guest_Gender",
            type: "int"
          },
          {
            name: "fldWBSet_Entry_Guest_GLNumber",
            type: "string"
          },
          {
            name: "fldWBSet_Entry_Guest_PlayingHandicap",
            type: "decimal"
          },
          {
            name: "fldWBSet_Entry_Guest_ClubName",
            type: "string"
          },
          {
            name: "fldWBSet_Entry_Guest_EmailAddress",
            type: "string"
          },
          {
            name: "fldWBSet_Entry_Guest_MobileNumber",
            type: "string"
          },
          {
            name: "fldWBSet_Entry_Guest_Notes",
            type: "string"
          }
        ]
      }
    },
    {
      execute:"ALTER TABLE tblWBSet_Entry ALTER COLUMN fldWBSet_Entry_Guest_PlayingHandicap DECIMAL(3, 1);"
    }

  ],
  down:[
    {
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_Name"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_Gender"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_GLNumber"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_PlayingHandicap"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_ClubName"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_EmailAddress"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_MobileNumber"
      },
      remove_column:{
        table:"tblWBSet_Entry",
        columnName: "fldWBSet_Entry_Guest_Notes"
      }
    }
  ]
}