﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Section",
        columns: [
          {
            name: "fldWB_Section_Options",
            type: "memo"
          }
        ]
      }
    },
    { add_column:{
      table: "tblWB_Booking",
      columns: [
        {
          name:"fldWB_Booking_Selected_Options",
          type:"memo"
        }
      ]
    }
    },
    {
      add_column:{
        table:"tblWB_Group",
        columns:[
          {
            name:"fldWB_Group_Options",
            type:"memo"
          }
        ]
      }
    }
  ],
  down:[
    {
      remove_column:{
        table:"tblWB_Section",
        columnName: "fldWB_Section_Options"
      },
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_Selected_Options"
      },
      remove_column:{
        table:"tblWB_Group",
        columnName:"fldWB_Group_Options"
      }
    }
  ]
}