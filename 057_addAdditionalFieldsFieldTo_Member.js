﻿{
    up: [
    {
        add_column: {
            table: "tblMember",
            columns: [
                {
                    name: "fldMember_AdditionalFields",
                    type: "memo"
                }
            ]
        }
    }
    ],

    down: [
    {
      remove_column: {
        table:"tblMember",
        columnName: "fldMember_AdditionalFields"
      }
    }
    ]
}