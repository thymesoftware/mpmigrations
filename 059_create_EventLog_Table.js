﻿{
    up:[
        {
          create_table:{
            name:"tblEventLog",
            columnPrefix:"fldEventLog_",
            columns:[
                {name:"ID",           type:"auto",    primaryKey:true},
                {name:"Timestamp",    type:"string"},
                {name:"SourceID",     type:"int"},
                {name:"EventName",    type:"string"},
                {name:"MemberID",     type:"int"},
                {name:"DetailsJSON",  type:"memo"}
            ]
          }
        }
    ],
    
    down:{
      drop_table:"tblEventLog"
    }
}