﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9203,999,null,"Code for the Occupation custom field", "", 0, ""],
                    [9204,999,null,"Code for the Emergency Contact Name custom field", "", 0, ""],
                    [9205,999,null,"Code for the Emergency Contact Number custom field", "", 0, ""],
                    [9206,999,null,"Code for the day pass count", "", 0, ""],
                    [9207,999,null,"Code for the day pass date", "", 0, ""],
                    [9208,999,null,"Category ID for day pass members", "", 0, ""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9203,
                  9204,
                  9205,
                  9206,
                  9207,
                  9208
                ]
            }
        }
    ]
}