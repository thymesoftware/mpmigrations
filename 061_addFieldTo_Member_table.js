﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_PosEnabled",
            type: "boolean"
          },
          {
            name: "fldMember_PosCreditLimit",
            type: "currency"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_PosEnabled"
      }
    },
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_PosCreditLimit"
      }
    }
  ]
}
