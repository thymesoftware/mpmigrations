﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_CustomInfo",
            type: "memo"
          }
        ]
      }
    },
    {
      insert_rows:{
        table_name:"tblCode",
        columnPrefix:"fldCode_",
        columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
        values:[
            [9209,999,null,"Code for the Australian Sailing Number custom field", "", 0, ""]
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_CustomInfo"
      }
    },
    {
      delete_rows: {
        table_name:"tblCode",
        compare_column:"fldCode_ID",
        values:[
          9209
        ]
      }
    }
  ]
}
