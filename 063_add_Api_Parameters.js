﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9210,999,null,"Valid IP Addresses for optional API restriction: Member Point", "", 0, ""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9210
                ]
            }
        }
    ]
}