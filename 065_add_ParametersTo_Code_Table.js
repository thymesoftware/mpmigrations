﻿{
    up:[
        {
            insert_rows:{
                table_name:"tblCode",
                columnPrefix:"fldCode_",
                columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
                values:[
                    [9015,999,null,"SMTP Authentication Username", "", 0, ""],
					[9016,999,null,"SMTP Authentication Password", "", 0, ""],
					[9017,999,null,"SMTP Authentication Port Number", "465", 0, ""]
                ]
            }
        }
    ],
    
    down:[
        {
            delete_rows:{
                table_name:"tblCode",
                compare_column:"fldCode_ID",
                values:[
                  9015,
				  9016,
				  9017
                ]
            }
        }
    ]
}