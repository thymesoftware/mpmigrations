{
    up:[

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'SMTP Server Address' WHERE fldCode_ID = 9010;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Email From Address' WHERE fldCode_ID = 9011;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Email From Name' WHERE fldCode_ID = 9012;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Reply-To Member Mode' WHERE fldCode_ID = 9013;"
        }
    ],
    
    down:[
		{
            execute:"UPDATE tblCode SET fldCode_Meaning = 'SMTP Server' WHERE fldCode_ID = 9010;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Booking Email From Address' WHERE fldCode_ID = 9011;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Booking Email From Name' WHERE fldCode_ID = 9012;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_Meaning = 'Booking Email Send From Member Mode' WHERE fldCode_ID = 9013;"
        }
    ]
}