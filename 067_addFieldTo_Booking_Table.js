﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Booking",
        columns: [
          {
            name: "fldWB_Booking_DeletedOn",
            type: "datetime"
          },
          {
            name: "fldWB_Booking_DeletedBy",
            type: "int"
          }
        ]
      }
    }
  ],
  down:[
    {
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_DeletedOn"
      },
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_DeletedBy"
      }
    }
  ]
}