﻿{
  up:[
      {
        execute:"CREATE NONCLUSTERED INDEX [IX_tblWB_Booking_Sequence] ON [tblWB_Booking] ( [fldWB_Booking_Sequence] ASC );"
      }
  ],
  down:[
    {
      execute:"IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = (SELECT OBJECT_ID('tblWB_Booking')) AND (name = 'IX_tblWB_Booking_Sequence')) DROP INDEX tblWB_Booking.IX_tblWB_Booking_Sequence;"
    }
  ]
}