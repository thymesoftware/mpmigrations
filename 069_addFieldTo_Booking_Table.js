﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Booking",
        columns: [
          {
            name: "fldWB_Booking_Guest_Address",
            type: "string"
          }
        ]
      }
    }
  ],
  down:[
    {
      remove_column:{
        table:"tblWB_Booking",
        columnName: "fldWB_Booking_Guest_Address"
      }
    }
  ]
}