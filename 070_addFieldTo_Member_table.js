﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_FinToDate",
            type: "date"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_FinToDate"
      }
    }
  ]
}
