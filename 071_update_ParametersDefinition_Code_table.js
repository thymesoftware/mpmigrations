{
    up:[
        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9203;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9204;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9205;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9206;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9207;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9208;"
        },

        {
            execute:"UPDATE tblCode SET fldCode_MMSync = 1 WHERE fldCode_ID = 9209;"
        }
    ],
    
    down:[
      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9203;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9204;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9205;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9206;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9207;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9208;"
      },

      {
        execute: "UPDATE tblCode SET fldCode_MMSync = 0 WHERE fldCode_ID = 9209;"
      }
    ]
}