﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_VaxDate",
            type: "date"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_VaxDate"
      }
    }
  ]
}
