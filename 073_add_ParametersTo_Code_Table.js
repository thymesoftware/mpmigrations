﻿{
  up: [
    {
      insert_rows: {
        table_name: "tblCode",
        columnPrefix: "fldCode_",
        columns: ["ID", "GroupID", "Sort", "Meaning", "DefaultValue", "MMSync", "Options"],
        values: [
          [110, 1, null, "Vax Status Restrictions", "", 0, ""],
          [5220, 110, null, "No Restriction", "", 0, ""],
          [5221, 110, null, "Vax Required", "", 0, ""]
        ]
      }
    }
  ],

    down: [
      {
        delete_rows: {
          table_name: "tblCode",
          compare_column: "fldCode_ID",
          values: [
            110,
            5220,
            5221
          ]
        }
      }
    ]
}