﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_VaxDoc",
            type: "string"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_VaxDoc"
      }
    }
  ]
}
