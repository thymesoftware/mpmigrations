﻿{
  up:[
      {
        insert_rows:{
          table_name:"tblCode",
          columnPrefix:"fldCode_",
          columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
          values:[
            [109,1,null,"Booking Sheet Mode Restrictions", "", 0, ""],
            [5110,109,null,"No Booking Mode Restriction", "", 0, ""],
            [5111,109,null,"Member Bookings Only", "", 0, ""],
            [5112, 109, null, "Public Bookings Only", "", 0, ""],
            [5203, 150, null, "Public Booking", "", null, ""]
          ]
        }
      }
  ],
    
  down:[
      {
        delete_rows:{
          table_name:"tblCode",
          compare_column:"fldCode_ID",
          values:[
            109,
            5110,
            5111,
            5112,
            5203
          ]
        }
      }
  ]
}