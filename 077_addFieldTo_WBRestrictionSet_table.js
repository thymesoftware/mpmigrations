{
	up: [
    {
		  add_column: {
			  table: "tblWB_RestrictionSet",
			  columns: [
          {
				    name: "fldWB_RestrictionSet_BookingMode",
				    type: "int"
			    }
        ]
		  }
	  }
  ],

	down: [
    {
		  remove_column: {
			  table: "tblWB_RestrictionSet",
			  columnName: "fldWB_RestrictionSet_BookingMode"
		  }
	  }
  ]
}
