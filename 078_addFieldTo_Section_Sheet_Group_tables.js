﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Section",
        columns: [
          {
            name: "fldWB_Section_BookingModeOverride",
            type: "int"
          }
        ]
      }
    },
    {
      add_column: {
        table: "tblWB_Sheet",
        columns: [
          {
            name: "fldWB_Sheet_BookingModeOverride",
            type: "int"
          }
        ]
      }
    },
    {
      add_column: {
        table: "tblWB_Group",
        columns: [
          {
            name: "fldWB_Group_BookingModeOverride",
            type: "int"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblWB_Section",
        columnName: "fldWB_Section_BookingModeOverride"
      }
    },
    {
      remove_column: {
        table: "tblWB_Sheet",
        columnName: "fldWB_Sheet_BookingModeOverride"
      }
    },
    {
      remove_column: {
        table: "tblWB_Group",
        columnName: "fldWB_Group_BookingModeOverride"
      }
    }
  ]
}
