﻿{
  up: [
    {
      create_table: {
        name: "tblReference_VisitorType",
        columnPrefix: "fldVisitorType_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "Name", type: "string" },
          { name: "Sort", type: "int" },
          { name: "Deleted", type: "bool" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_Asset",
        columnPrefix: "fldAsset_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "Name", type: "string" },
          { name: "PriceLevelContextID", type: "int" },
          { name: "TypeCode", type: "int" },
          { name: "Sort", type: "int" },
          { name: "Details", type: "memo" },
          { name: "Deleted", type: "bool" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_Addon",
        columnPrefix: "fldAddon_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "Name", type: "string" },
          { name: "PriceLevelContextID", type: "int" },
          { name: "TypeCode", type: "int" },
          { name: "Sort", type: "int" },
          { name: "Details", type: "memo" },
          { name: "Deleted", type: "bool" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_PriceLevelContext",
        columnPrefix: "fldPriceLevelContext_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "Description", type: "string" },
          { name: "SpecialPurposeID", type: "int" },
          { name: "Deleted", type: "bool" },
          { name: "Details", type: "memo" },
          { name: "Prices", type: "memo" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_PriceLevel",
        columnPrefix: "fldPriceLevel_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "ContextID", type: "int" },
          { name: "Name", type: "string" },
          { name: "Default", type: "bool" },
          { name: "Deleted", type: "bool" },
          { name: "Sort", type: "int" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_PriceLevelLine",
        columnPrefix: "fldPriceLevelLine_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "ContextID", type: "int" },
          { name: "PriceLevelID", type: "int" },
          { name: "Deleted", type: "bool" },
          { name: "FromDate", type: "date" },
          { name: "ExpiryDate", type: "date" },
          { name: "Specification", type: "memo" }
        ]
      }
    },
    {
      create_table: {
        name: "tblReference_PublicHolidays",
        columnPrefix: "fldPublicHoliday_",
        columns: [
          { name: "ID", type: "int", primaryKey: true },
          { name: "Auto", type: "bool" },
          { name: "Jurisdiction", type: "string" },
          { name: "Year", type: "int" },
          { name: "KeyName", type: "string" },
          { name: "FullName", type: "string" },
          { name: "Date", type: "date" },
          { name: "Information", type: "memo" },
          { name: "Link", type: "string" },
          { name: "Enabled", type: "bool" },
          { name: "Deleted", type: "bool" }
        ]
      }
    }
  ],

  down: [
    {
      drop_table: "tblReference_VisitorType"
    },
    {
      drop_table: "tblReference_Asset"
    },
    {
      drop_table: "tblReference_Addon"
    },
    {
      drop_table: "tblReference_PriceLevelContext"
    },
    {
      drop_table: "tblReference_PriceLevel"
    },
    {
      drop_table: "tblReference_PriceLevelLine"
    },
    {
      drop_table: "tblReference_PublicHolidays"
    }
  ]
}