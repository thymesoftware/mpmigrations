﻿{
  up:[
      {
        insert_rows:{
          table_name:"tblCode",
          columnPrefix:"fldCode_",
          columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
          values: [
            [111, 1, null, "Hole Mode Options", "", null, ""],
            [5230, 111, null, "Dont Care", "", null, ""],
            [5231, 111, null, "18 Hole", "", null, ""],
            [5232, 111, null, "9 Hole", "", null, ""],
            [5233, 111, null, "9 And 18 Hole", "", null, ""]
          ]
        }
      }
  ],
    
  down:[
      {
        delete_rows:{
          table_name:"tblCode",
          compare_column:"fldCode_ID",
          values: [
            111,
            5230,
            5231,
            5232,
            5233
          ]
        }
      }
  ]
}