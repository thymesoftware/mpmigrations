﻿{
  up: [
    {
      add_column: {
        table: "tblWB_Booking",
        columns: [
          {
            name: "fldWB_Booking_HoleMode",
            type: "int"
          }
        ]
      }
    },
    {
      add_column: {
        table: "tblWB_Sheet",
        columns: [
          {
            name: "fldWB_Sheet_HoleModeOptions",
            type: "int"
          }
        ]
      }
    },
    {
      add_column: {
        table: "tblWB_Section",
        columns: [
          {
            name: "fldWB_Section_HoleModeOptions",
            type: "int"
          }
        ]
      }
    },
    {
      add_column: {
        table: "tblWB_Group",
        columns: [
          {
            name: "fldWB_Group_HoleModeOptions",
            type: "int"
          }
        ]
      }
    }
  ],
  down: [
    {
      remove_column: {
        table: "tblWB_Booking",
        columnName: "fldWB_Booking_HoleMode"
      }
    },
    {
      remove_column: {
        table: "tblWB_Sheet",
        columnName: "fldWB_Sheet_HoleModeOptions"
      }
    },
    {
      remove_column: {
        table: "tblWB_Section",
        columnName: "fldWB_Section_HoleModeOptions"
      }
    },
    {
      remove_column: {
        table: "tblWB_Group",
        columnName: "fldWB_Group_HoleModeOptions"
      }
    }
  ]
}