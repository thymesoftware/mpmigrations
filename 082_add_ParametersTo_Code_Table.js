﻿{
  up:[
      {
        insert_rows:{
          table_name:"tblCode",
          columnPrefix:"fldCode_",
          columns:["ID", "GroupID", "Sort", "Meaning","DefaultValue", "MMSync", "Options"],
          values: [
            [9054, 999, null, "Min Vax Age", "", 1, ""],
            [9055, 999, null, "Use Email For Member Login", "", 1, ""]
          ]
        }
      }
  ],
    
  down:[
      {
        delete_rows:{
          table_name:"tblCode",
          compare_column:"fldCode_ID",
          values: [
            9054,
            9055
          ]
        }
      }
  ]
}