﻿{
  up: [
    {
      add_column: {
        table: "tblMember",
        columns: [
          {
            name: "fldMember_ExtraDetailsFms",
            type: "memo"
          },
          {
            name: "fldMember_EmergencyContactFms",
            type: "memo"
          },
          {
            name: "fldMember_AustSailingNumber",
            type: "string"
          }
        ]
      }
    }
  ],

  down: [
    {
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_ExtraDetailsFms"
      },
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_EmergencyContactFms"
      },
      remove_column: {
        table: "tblMember",
        columnName: "fldMember_AustSailingNumber"
      }
    }
  ]
}
